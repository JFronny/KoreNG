package gov.nsa.kore.ng.model;

import gov.nsa.kore.ng.util.RegexUtil;
import io.gitlab.jfronny.commons.data.dynamic.DFinal;
import io.gitlab.jfronny.commons.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.ExpressionParameter;
import io.gitlab.jfronny.muscript.StandardLib;

import java.util.*;

public record EvaluationParameter(List<String> parameters, Map<String, Dynamic<?>> globals) {
    public EvaluationParameter fork() {
        return new EvaluationParameter(new LinkedList<>(parameters), globals);
    }

    public EvaluationParameter(String input, Map<String, Dynamic<?>> globals) {
        this(new LinkedList<>(Set.of(input)), globals);
    }

    public ExpressionParameter toDynamic() {
        ExpressionParameter expr = new ExpressionParameter();
        StandardLib.addTo(expr);
        expr.set("global", DFinal.of(globals));
        for (int i = 0, parametersSize = parameters().size(); i < parametersSize; i++) {
            expr.set("p" + i, buildValue(parameters().get(i)));
        }
        return expr;
    }

    private Dynamic<?> buildValue(String content) {
        if (RegexUtil.DOUBLE.test(content))
            return DFinal.of(Double.parseDouble(content));
        if (RegexUtil.BOOL.test(content))
            return DFinal.of(RegexUtil.TRUE.test(content));
        return DFinal.of(content);
    }
}
