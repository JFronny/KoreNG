package gov.nsa.kore.ng.model;

import gov.nsa.kore.ng.Main;
import gov.nsa.kore.ng.model.node.base.AINode;

import java.util.Optional;

public record EvaluationResult(boolean success, Optional<String> result, Optional<AINode> continueNode) {
    public static EvaluationResult fail(String message) {
        return new EvaluationResult(false, Optional.of(message), Optional.empty());
    }

    public static EvaluationResult fail(Throwable t) {
        return fail(Main.LOG.format(t));
    }

    public static EvaluationResult success(String message) {
        return success(message, null);
    }

    public static EvaluationResult success(String message, AINode continueNode) {
        return new EvaluationResult(true, Optional.ofNullable(message), Optional.ofNullable(continueNode));
    }

    public EvaluationResult orContinue(AINode continueNode) {
        if (this.continueNode.isPresent()) return this;
        return new EvaluationResult(success, result, success ? Optional.ofNullable(continueNode) : Optional.empty());
    }
}
