package gov.nsa.kore.ng.model.node;

import gov.nsa.kore.ng.model.*;
import gov.nsa.kore.ng.model.node.base.ScriptAINode;
import io.gitlab.jfronny.muscript.compiler.expr.Expr;
import io.gitlab.jfronny.muscript.compiler.expr.StringExpr;
import io.gitlab.jfronny.muscript.error.LocationalException;

public class OptionNode extends ScriptAINode<StringExpr> {
    @Override
    public EvaluationResult evaluateImpl(String input, EvaluationParameter parameters) throws EvaluationException {
        try {
            return EvaluationResult.success(script.get(parameters.toDynamic()), getContinueNode());
        }
        catch (Throwable t) {
            if (t instanceof LocationalException le) {
                throw new EvaluationException("Could not execute starscript: " + le.asPrintable(convertedSource).toString());
            }
            throw new EvaluationException("Could not execute starscript", t);
        }
    }

    public static class Builder extends ScriptAINode.Builder<StringExpr, OptionNode> {
        @Override
        protected OptionNode getInstance() {
            return new OptionNode();
        }

        @Override
        protected StringExpr coerceExpr(Expr<?> expr) {
            return expr.asStringExpr().optimize();
        }
    }
}
