package gov.nsa.kore.ng.model.node;

import gov.nsa.kore.ng.model.*;
import gov.nsa.kore.ng.model.node.base.ScriptAINode;
import io.gitlab.jfronny.muscript.compiler.expr.DynamicExpr;
import io.gitlab.jfronny.muscript.compiler.expr.Expr;
import io.gitlab.jfronny.muscript.error.LocationalException;

public class StoreNode extends ScriptAINode<DynamicExpr> {
    @Override
    protected EvaluationResult evaluateImpl(String input, EvaluationParameter parameters) throws EvaluationException {
        try {
            parameters.globals().put(getId(), script.get(parameters.toDynamic()));
            return EvaluationResult.success(null, getContinueNode());
        }
        catch (Throwable t) {
            if (t instanceof LocationalException le) {
                throw new EvaluationException("Could not execute starscript: " + le.asPrintable(convertedSource).toString());
            }
            throw new EvaluationException("Could not execute starscript", t);
        }
    }

    public static class Builder extends ScriptAINode.Builder<DynamicExpr, StoreNode> {
        @Override
        protected StoreNode getInstance() {
            return new StoreNode();
        }

        @Override
        protected DynamicExpr coerceExpr(Expr<?> expr) {
            return expr.asDynamicExpr().optimize();
        }
    }
}
