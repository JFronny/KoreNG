package gov.nsa.kore.ng.model.node.base;

import gov.nsa.kore.ng.util.xml.XmlException;
import io.gitlab.jfronny.muscript.StarScriptIngester;
import io.gitlab.jfronny.muscript.compiler.Parser;
import io.gitlab.jfronny.muscript.compiler.expr.Expr;

public abstract class ScriptAINode<T extends Expr<?>> extends AINode {
    protected T script;
    protected String scriptSource;
    protected String convertedSource;

    public String getScriptSource() {
        return scriptSource;
    }

    public static abstract class Builder<K extends Expr<?>, T extends ScriptAINode<K>> extends AINode.Builder<T> {
        private String scriptSource = "";

        public void setScriptSource(String scriptSource) {
            this.scriptSource = scriptSource;
        }

        abstract protected K coerceExpr(Expr<?> expr);

        @Override
        public T build() throws XmlException {
            T node = super.build();
            node.scriptSource = scriptSource;
            node.convertedSource = StarScriptIngester.starScriptToMu(scriptSource);
            try {
                node.script = coerceExpr(Parser.parse(node.convertedSource));
            } catch (Parser.ParseException pe) {
                throw new XmlException("Discovered the following issues in the starscript \"" +
                        scriptSource +
                        "\" (converted to \"" +
                        node.convertedSource +
                        "\"):" +
                        pe.error.toString());
            }
            return node;
        }
    }
}
